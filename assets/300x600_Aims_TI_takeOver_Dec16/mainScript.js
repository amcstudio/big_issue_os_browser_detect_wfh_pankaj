function setValues() {
    TweenMax.set(headline, { x: -300 }), 
    TweenMax.set(mainAnim, { scale: 1.1 }), 
    anim1()
}

function anim1() {
    TweenMax.to(startPlate, .7, { opacity: 0 }), 
    TweenMax.to(startPlate, .7, { left: 1e5, delay: copyDur }), 
    TweenMax.to(vertBar_1, .8, {left: "2px", scaleX: 1, delay: mainDelay + .2, ease: Power4.easeOut }), 
    TweenMax.to(vertBar_2, .8, {left: "4px", scaleX: 1, delay: mainDelay + .3, ease: Power4.easeOut }), 
    TweenMax.to(vertBar_3, .8, {left: "6px", scaleX: 1, delay: mainDelay + .5, ease: Power4.easeOut }), 
    TweenMax.to(vertBar_4, .8, {left: "12px", scaleX: 1, delay: mainDelay + .6, ease: Power4.easeOut }), 
    TweenMax.to(vertBar_5, .8, {left: "14px", scaleX: 1, delay: mainDelay + .8, ease: Power4.easeOut }), 
    TweenMax.to(vertBar_6, .8, {left: "16px", scaleX: 1, delay: mainDelay + 1, ease: Power4.easeOut }), 
    TweenMax.to(headline, 1.5, {x: 0, opacity: 1, ease: Quad.easeOut, delay: 1, onStart: anim3 }), 
    TweenMax.to(mainAnim, 5, {scale: 1, rotation: .01, ease: Quad.easeInOut }), 
    TweenMax.to(stripeMask1, stripeSpeed, {height: 251, roation: .01, delay: stripeDelay, ease: Quad.easeOut }), 
    TweenMax.to(stripeMask2, stripeSpeed, {height: 251, roation: .01, delay: stripeDelay + .1, ease: Quad.easeOut }), 
    TweenMax.to(stripeMask3, stripeSpeed, {height: 251, roation: .01, delay: stripeDelay + .2, ease: Quad.easeOut }), 
    TweenMax.to(stripeMask4, stripeSpeed, {height: 251, roation: .01, delay: stripeDelay + .3, ease: Quad.easeOut }), 
    TweenMax.to(stripeMask5, stripeSpeed, {height: 251, roation: .01, delay: stripeDelay + .4, ease: Quad.easeOut })
}

function anim3() {
    myTimeout = setTimeout(holdABit, 1700)
}

function holdABit() {
    splitDiv("heroLine", .4, 4, !0, !0, !0, .89), myTimeout = setTimeout(anim4, 5700)
}

function anim4() {
    splitDiv("copy2", .46, 4, !1, !1, !1, .93), pulseAnim = setInterval(doPulseAnim, 1500), setTimeout(function() {
        clearInterval(pulseAnim)
    }, 5e3), TweenMax.to(cta, 1, {
        opacity: 1,
        ease: Quad.easeOut,
        delay: 1
    })
}

function doPulseAnim() {
    TweenMax.set(cta, { boxShadow: "0px 0px 0px 0px rgba(255, 217, 0, 1)" }), 
    TweenMax.to(cta, 1.25, { boxShadow: "0px 0px 8px 20px rgba(255, 217, 0, 0)", ease: Power4.easeOut })
}

function openLegals() {
    TweenMax.fromTo(legalsPanel, 1, {
        top: "600px"
    }, {
        top: "0px",
        ease: Power2.easeOut,
        onStart: function() {
            legalsPanel.style.opacity = 1
        }
    }), Enabler.counter("Open legals")
}

function closeLegals() {
    TweenMax.fromTo(legalsPanel, 1, {
        top: "0"
    }, {
        top: "600px",
        ease: Power2.easeOut,
        onComplete: function() {
            legalsPanel.style.opacity = 0
        }
    })
}

function exitBanner() {
    TweenMax.killAll(!1, !0, !0, !0), clearTimeout(myTimeout), TweenMax.set(vertBar_1, {left: "2px", scaleX: 1 }), 
    TweenMax.set(vertBar_2, {left: "4px", scaleX: 1 }), 
    TweenMax.set(vertBar_3, {left: "6px", scaleX: 1 }), 
    TweenMax.set(vertBar_4, {left: "12px", scaleX: 1 }), 
    TweenMax.set(vertBar_5, {left: "14px", scaleX: 1 }), 
    TweenMax.set(vertBar_6, {left: "16px", scaleX: 1 }), 
    TweenMax.set(cta, {opacity: 1 }), 
    TweenMax.set(headline, {x: 0, opacity: 1 }), 
    TweenMax.set(mainAnim, {x: 0, y: 0, scale: 1 }), 
    TweenMax.set(heroLine, {opacity: 0 }), 
    TweenMax.set(copy2, {opacity: 1 }), 
    TweenMax.set(stripeMask1, {height: 251 }), 
    TweenMax.set(stripeMask2, {height: 251 }), 
    TweenMax.set(stripeMask3, {height: 251 }), 
    TweenMax.set(stripeMask4, {height: 251 }), 
    TweenMax.set(stripeMask5, {height: 251 })
}
for (var container, content, bgExit, endFrame = !1, unitWidth = 300, tweenDist = 30, pulseAnim, copyHold = 3, copyDur = .8, stagger = .3, mainDelay = 0, darkenerOpac = .7, myTimeout, icePartAngle = 5, stripeDelay = 2.7, stripeSpeed = 5, elementArr = document.getElementsByClassName("elems"), clickThrough = document.getElementById("clickThrough"), i = 0; i < elementArr.length; i++) window[elementArr[i]] = document.getElementById(elementArr[i]);
init = function() {
    container = document.getElementById("container_dc"), content = document.getElementById("content_dc"), TweenMax.set(container, {
        perspective: 1e3
    }), addListeners(), container.style.display = "block";
    var e = 6;
    for (i = 1; e >= i; i++) {
        var t = document.getElementById("vertBar_" + i);
        t.style.left = document.body.clientWidth + 100 + "px", TweenMax.set(t, {
            scaleX: 10
        })
    }
    container.style.opacity = 1
}, addListeners = function() {
    clickThrough.addEventListener("click", bgExitHandler, !1), disc.addEventListener("click", openLegals, !1), legalsPanel.addEventListener("click", closeLegals, !1)
}, setValues(), bgExitHandler = function(e) {
    Enabler.exit("Main clickthrough"), exitBanner()
}, init();